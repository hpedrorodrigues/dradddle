package com.hpedrorodrigues.dradddle.view.activity

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.graphics.Palette
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.Toast
import com.hpedrorodrigues.dradddle.R
import com.hpedrorodrigues.dradddle.constant.DradddleConstants
import com.hpedrorodrigues.dradddle.entity.Shot
import com.hpedrorodrigues.dradddle.network.DradddleNetwork
import com.hpedrorodrigues.dradddle.observable.NetworkStateObservable
import com.hpedrorodrigues.dradddle.service.ConnectionService
import com.hpedrorodrigues.dradddle.util.*
import com.hpedrorodrigues.dradddle.view.adapter.CommentsAdapter
import com.squareup.picasso.Callback
import com.yoavst.kotlin.beforeLollipop
import com.yoavst.kotlin.e
import kotlinx.android.synthetic.activity_shot.background_image
import kotlinx.android.synthetic.activity_shot.collapsing_toolbar
import kotlinx.android.synthetic.activity_shot.super_recycler_view
import kotlinx.android.synthetic.activity_shot.toolbar
import kotlinx.android.synthetic.activity_shot.shot_button
import kotlinx.android.synthetic.activity_shot.coordinator
import kotlinx.android.synthetic.activity_shot.more_progress
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*
import javax.inject.Inject
import kotlin.platform.platformStatic

public class ShotActivity() : BaseActivity() {

    companion object {

        platformStatic val ITEM_LEFT_TO_LOAD_MORE = 5
        platformStatic val MAX_RETRIES = 3L
        platformStatic val INIT_PAGE = 0
    }

    var dradddleNetwork: DradddleNetwork? = null
        @Inject set

    var commentsAdapter: CommentsAdapter? = null
        @Inject set

    var connectionService: ConnectionService? = null
        @Inject set

    private var actualPage: Int = INIT_PAGE
    private var canHasMore: Boolean = true

    private val loadImageCallback = object: Callback {
        override fun onSuccess() {
            hideProgress()
            val bitmap = (background_image.getDrawable() as BitmapDrawable).getBitmap()
            Palette.from(bitmap).generate({ palette ->
                val swatch = PaletteUtil.getSwatch(palette)
                collapsing_toolbar.setContentScrim(ColorDrawable(swatch.getRgb()))

                val darkColor = ColorUtil.getDarkerColor(swatch.getRgb())
                shot_button.setBackgroundTintList(ColorStateList.valueOf(darkColor))

                if (!beforeLollipop()) {
                    getWindow().setStatusBarColor(darkColor)
                    getWindow().setNavigationBarColor(darkColor)
                }
            })
        }
        override fun onError() {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shot)

        if (!connectionService!!.hasConnection()) {
            Toast.makeText(this, R.string.connection_is_required_for_full_view_of_the_shot,
                    Toast.LENGTH_LONG).show()
            finish()
        } else {
            showProgress()
            configToolbar()
            configSuperRecyclerView()
            loadShot()
        }
    }

    override fun injectMembers() {
        dradddleComponent().inject(this)
    }

    private fun configToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        getSupportActionBar().setDisplayHomeAsUpEnabled(true)
        setTranslucentToolbar()
        (toolbar as Toolbar).setTitle(getString(R.string.app_name))
    }

    private fun setTranslucentToolbar() {
        (toolbar.getBackground() as ColorDrawable)
                .setAlpha(DradddleConstants.MIN_VALUE_ALPHA.toInt())
    }

    private fun loadShot() {
        val shotId = getIntent().getLongExtra(DradddleConstants.SHOT_ID, 0)
        val subscription = dradddleNetwork!!.retrieveShot(shotId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showShotInfo(it)
                    loadNextPageComments()
                }, {
                    Snackbar.make(super_recycler_view,
                            R.string.connection_is_required_for_full_view_of_the_shot,
                            Snackbar.LENGTH_LONG)
                            .setAction(android.R.string.ok, {})
                            .show()
                })
        compositeSubscription.add(subscription)
    }

    private fun showShotInfo(shot: Shot) {
        DradddlePicasso.loadShot(shot.imageUrl!!).into(background_image, loadImageCallback)
        collapsing_toolbar.setTitle(shot.title)

        shot_button.setOnClickListener {
            DradddleApp.openBrowser(this, shot.url!!)
        }

        background_image.setOnClickListener {
            val intent = Intent(this, javaClass<ShotViewActivity>())
            intent.putExtra(DradddleConstants.SHOT_IMAGE_URL, shot.imageUrl!!)
            startActivity(intent)
            overrideTransitionWithFade()
        }
    }

    private fun configSuperRecyclerView() {
        super_recycler_view.setLayoutManager(LinearLayoutManager(super_recycler_view!!.getContext()))
        super_recycler_view.setAdapter(commentsAdapter)

        super_recycler_view.setupMoreListener({ numberOfItems, numberBeforeMore, currentItemPos ->
            if (canHasMore) {
                loadNextPageComments()
            } else {
                super_recycler_view!!.hideMoreProgress()
            }
        }, ITEM_LEFT_TO_LOAD_MORE)
    }

    private fun loadNextPageComments() {
        if (connectionService!!.hasConnection()) showLoadingView() else showWithoutNetworkView()

        actualPage++

        val shotId = getIntent().getLongExtra(DradddleConstants.SHOT_ID, 0)

        val subscription = dradddleNetwork!!.retrieveComments(shotId, actualPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(MAX_RETRIES)
                .subscribe({
                    commentsAdapter!!.addPage(it)
                    canHasMore = it.total!! > commentsAdapter!!.getItemCount()
                }, {
                    Snackbar.make(super_recycler_view,
                            R.string.connection_is_required_for_full_view_of_the_shot,
                            Snackbar.LENGTH_LONG)
                            .setAction(android.R.string.ok, {})
                            .show()
                }, {
                    super_recycler_view!!.hideMoreProgress()
                    hideProgress()

                    val hasComments = commentsAdapter!!.getItemCount() > 0
                    if (connectionService!!.hasConnection() && !hasComments) {
                        showWithoutCommentsView()
                    }
                })
        compositeSubscription.add(subscription)
    }

    private fun showWithoutCommentsView() {
        val emptyView = super_recycler_view.getEmptyView()
        emptyView.findViewById(R.id.without_comments).setVisibility(View.VISIBLE)
        emptyView.findViewById(R.id.without_network).setVisibility(View.GONE)
        emptyView.findViewById(R.id.loading).setVisibility(View.GONE)
    }

    private fun showLoadingView() {
        val emptyView = super_recycler_view.getEmptyView()
        emptyView.findViewById(R.id.without_comments).setVisibility(View.GONE)
        emptyView.findViewById(R.id.without_network).setVisibility(View.GONE)
        emptyView.findViewById(R.id.loading).setVisibility(View.VISIBLE)
    }

    private fun showWithoutNetworkView() {
        val emptyView = super_recycler_view.getEmptyView()
        emptyView.findViewById(R.id.without_network).setVisibility(View.VISIBLE)
        emptyView.findViewById(R.id.without_comments).setVisibility(View.GONE)
        emptyView.findViewById(R.id.loading).setVisibility(View.GONE)
    }

    private fun showProgress() {
        coordinator.setVisibility(View.GONE)
        more_progress.setVisibility(View.VISIBLE)
    }

    private fun hideProgress() {
        coordinator.setVisibility(View.VISIBLE)
        more_progress.setVisibility(View.GONE)
    }
}