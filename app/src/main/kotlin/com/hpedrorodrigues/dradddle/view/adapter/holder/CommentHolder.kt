package com.hpedrorodrigues.dradddle.view.adapter.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.hpedrorodrigues.dradddle.R
import de.hdodenhof.circleimageview.CircleImageView

public class CommentHolder(val view: View) : RecyclerView.ViewHolder(view) {

    val authorAvatar: CircleImageView
    val authorName: TextView
    val comment: TextView
    val likesCount: TextView

    init {
        this.authorAvatar = view.findViewById(R.id.author_avatar) as CircleImageView
        this.authorName = view.findViewById(R.id.author_name) as TextView
        this.comment = view.findViewById(R.id.comment) as TextView
        this.likesCount = view.findViewById(R.id.likes_count) as TextView
    }
}