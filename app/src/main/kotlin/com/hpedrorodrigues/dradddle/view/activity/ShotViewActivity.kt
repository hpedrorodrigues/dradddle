package com.hpedrorodrigues.dradddle.view.activity

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.v7.graphics.Palette
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.Window
import android.view.WindowManager

import com.hpedrorodrigues.dradddle.R
import com.hpedrorodrigues.dradddle.constant.DradddleConstants
import com.hpedrorodrigues.dradddle.util.ColorUtil
import com.hpedrorodrigues.dradddle.util.DradddlePicasso
import com.hpedrorodrigues.dradddle.util.PaletteUtil
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.yoavst.kotlin.beforeLollipop

import it.sephiroth.android.library.imagezoom.ImageViewTouch
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase

import kotlinx.android.synthetic.activity_shot_view.image_touch

public class ShotViewActivity : BaseActivity() {

    private val loadImageCallback = object: Target {

        override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
            image_touch.setImageBitmap(bitmap)
            Palette.from(bitmap).generate({ palette ->
                val vibrantSwatch = PaletteUtil.getSwatch(palette)
                if (!beforeLollipop()) {
                    val darkColor = ColorUtil.getDarkerColor(vibrantSwatch.getRgb())
                    getWindow().setStatusBarColor(darkColor)
                    getWindow().setNavigationBarColor(darkColor)
                }
            })
        }

        override fun onBitmapFailed(errorDrawable: Drawable) {
            image_touch.setImageDrawable(errorDrawable)
        }

        override fun onPrepareLoad(placeHolderDrawable: Drawable) {
            image_touch.setImageDrawable(placeHolderDrawable)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setFullScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shot_view)

        val shotUrl = getIntent().getStringExtra(DradddleConstants.SHOT_IMAGE_URL)

        image_touch.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN)

        DradddlePicasso.loadShot(shotUrl).into(loadImageCallback)
    }

    private fun setFullScreen() {
        if (beforeLollipop()) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN)
        } else {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                            View.SYSTEM_UI_FLAG_FULLSCREEN or
                            View.SYSTEM_UI_FLAG_IMMERSIVE)
        }
    }

    override fun injectMembers() {
        dradddleComponent().inject(this)
    }
}