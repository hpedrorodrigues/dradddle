package com.hpedrorodrigues.dradddle.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import com.hpedrorodrigues.dradddle.R
import com.hpedrorodrigues.dradddle.entity.Comment
import com.hpedrorodrigues.dradddle.entity.Page
import com.hpedrorodrigues.dradddle.util.DradddlePicasso
import com.hpedrorodrigues.dradddle.view.adapter.holder.CommentHolder
import java.util.*
import javax.inject.Inject

public class CommentsAdapter : RecyclerView.Adapter<CommentHolder> {

    var inflater: LayoutInflater? = null
        @Inject set

    var context: Context? = null
        @Inject set

    private var comments: MutableList<Comment> = ArrayList<Comment>()

    @Inject
    constructor() : super() {}

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CommentHolder {
        return CommentHolder(inflater!!.inflate(R.layout.comment, parent, false))
    }

    override fun onBindViewHolder(holder: CommentHolder, position: Int) {
        val comment = comments.get(position)

        holder.authorName.setText(comment.player!!.name)
        holder.comment.setText(Html.fromHtml(comment.body))
        holder.likesCount.setText(comment.likesCount.toString())

        DradddlePicasso.loadAvatar(comment.player!!.avatarUrl!!).into(holder.authorAvatar)
    }

    override fun getItemCount(): Int = comments.size()

    public fun addPage(page: Page) {
        comments.addAll(page.comments!!)
        notifyDataSetChanged()
    }
}