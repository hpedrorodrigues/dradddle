package com.hpedrorodrigues.dradddle.util

import android.support.v7.graphics.Palette

public object PaletteUtil {

    public fun getSwatch(palette: Palette): Palette.Swatch {
        var swatch = palette.getVibrantSwatch()

        if (swatch == null) {
            swatch = palette.getDarkVibrantSwatch()
        }
        if (swatch == null) {
            swatch = palette.getLightVibrantSwatch()
        }
        if (swatch == null) {
            swatch = palette.getSwatches().get(0)
        }
        return swatch
    }
}