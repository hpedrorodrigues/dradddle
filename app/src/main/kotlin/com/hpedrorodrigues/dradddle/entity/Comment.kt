package com.hpedrorodrigues.dradddle.entity

import com.google.gson.annotations.SerializedName

public class Comment {

    var id: Long? = null

    SerializedName("likes_count")
    var likesCount: Int? = null

    var body: String? = null

    var player: Player? = null
}