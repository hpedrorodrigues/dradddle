# Screenshots

## Home

<p align="center">
    <img src="./screenshots/home_1.png?raw=true" height="512" width="288"/>
    <img src="./screenshots/home_2.png?raw=true" height="512" width="288"/>
</p>

## Settings

<p align="center">
    <img src="./screenshots/settings_1.png?raw=true" height="512" width="288"/>
    <img src="./screenshots/settings_2.png?raw=true" height="512" width="288"/>
</p>

## About

<p align="center">
    <img src="./screenshots/about_1.png?raw=true" height="512" width="288"/>
    <img src="./screenshots/about_2.png?raw=true" height="512" width="288"/>
</p>

## Dribbble

<p align="center">
    <img src="./screenshots/dribbble_1.png?raw=true" height="512" width="288"/>
    <img src="./screenshots/dribbble_2.png?raw=true" height="512" width="288"/>
</p>

## Shot details

<p align="center">
    <img src="./screenshots/shot_detail_1.png?raw=true" height="512" width="288"/>
    <img src="./screenshots/shot_detail_2.png?raw=true" height="512" width="288"/>
</p>

## Shot image

<p align="center">
    <img src="./screenshots/shot_image_1.png?raw=true" height="512" width="288"/>
    <img src="./screenshots/shot_image_2.png?raw=true" height="512" width="288"/>
</p>